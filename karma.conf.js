module.exports = function (config) {
    config.set({
        browsers: ['PhantomJS'],
        frameworks: ['jasmine'],
        files: [
            './tests/**/*.js'
        ],
		reporters: ['mocha', 'coverage', 'html'],
        coverageReporter: {
            dir: 'coverage/',
            reporters: [
				{ type: 'html', subdir: 'report-html' },
				{ type: 'lcov', subdir: 'report-lcov' },
				{ type: 'cobertura', subdir: 'report-jenkins'},
				{ type: 'text-summary' }
			],
            instrumenterOptions: {
                istanbul: {
                    noCompact: true
                }
            }
        },
		htmlReporter: {
			outputDir: 'karma_html', // where to put the reports  
			templatePath: null, // set if you moved jasmine_template.html 
			focusOnFailures: true, // reports show failures on start 
			namedFiles: false, // name files instead of creating sub-directories 
			pageTitle: null, // page title for reports; browser info by default 
			urlFriendlyName: false, // simply replaces spaces with _ for files/dirs 
			reportName: 'report-summary-filename', // report summary filename; browser info by default 
			  
			// experimental 
			preserveDescribeNesting: true, // folded suites stay folded  
			foldAll: true, // reports start folded (only with preserveDescribeNesting) 
		},
        autoWatch: true,
        singleRun: false,
        plugins: [
            require('karma-jasmine'),
            require('karma-phantomjs-launcher'),
            require('karma-mocha'),
            require('karma-chai'),
            require('karma-coverage'),
            require('karma-mocha-reporter'),
			require('karma-html-reporter'),
            require('karma-sourcemap-loader')
        ]
    });
};